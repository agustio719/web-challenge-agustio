import { API, INSTANCE } from '@/configs/api';

export const getProducts = async ({ params }) =>{

    try{
        const res = await INSTANCE.get(API.product, { params })
        return { data: res.data , status: res.status }
    }
    catch(error){
        return { error: error.response.data.message }
    }
       
}

export const getProductsByCategory = async ({ category }) =>{
    try{
        const res = await INSTANCE.get(API.product + '/category/' + category)
        return { data: res.data , status: res.status }
    }
    catch(error){
        return { error: error }
    }
       
}

export const getProductBySearch = async ({ params }) =>{
    try{
        const res = await INSTANCE.get(API.product + '/search/', { params })
        return { data: res.data , status: res.status }
    }
    catch(error){
        return { error: error }
    }
       
}

export const getCategory = async () =>{
    try{
        const res = await INSTANCE.get(API.product + '/categories')
        return { data: res.data , status: res.status }
    }
    catch(error){
        return { error: error.response.data.message }
    }
       
}


export const getProductById = async ({ id }) => {
    try{
        const res = await INSTANCE.get(API.product + `/${id}`)
        return { data: res.data , status: res.status }
    }
    catch(error){
        return { error: error.response.data.message }
    }
}