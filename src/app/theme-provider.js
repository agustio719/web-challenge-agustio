'use client';
import { createContext, useState, useEffect } from 'react';
import Cookies from 'js-cookie';

export const ThemeContext = createContext();

export const ThemeProvider = ({ children }) => {
    const [state, setState] = useState({
        user : null,
    });
    const { user } = state;
    

    const setUser = (user) => {
        Cookies.set('user', JSON.stringify(user));
        setState({ ...state, user });
    }

    useEffect(() => {
        const user = Cookies.get('user')
        if (user) {
            setState({ ...state, user:JSON.parse(user) });
        }
    }, []);

   
    return (
        <ThemeContext.Provider value={{ user , setUser }}>
            <div className="h-auto w-screen min-h-screen">
                {children}
            </div>
        </ThemeContext.Provider>
    );
    }