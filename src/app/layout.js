import './globals.css'
import { Inter } from 'next/font/google'
import { ThemeProvider } from './theme-provider'

const inter = Inter({
  subsets: ['latin'],
  display: 'swap',
})
 
export const metadata = {
    title: 'Infosys',
    description: 'Infosys',
  }
export default function RootLayout({ children }) {
  
    return (
    <html lang="en" className={inter.className}>
      <body>
        <main>
          <ThemeProvider>
            {children}
          </ThemeProvider>
        </main>
      </body>
    </html>
  )
}