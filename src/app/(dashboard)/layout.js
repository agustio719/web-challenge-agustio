import Navbar from '@/components/clients/navbar/navbar';

const Layout = ({ children }) => {
    return (
        <div className="min-h-screen">
            <Navbar />
            <div className="h-full">
                {children}
            </div>
        </div>
    );
}

export default Layout;