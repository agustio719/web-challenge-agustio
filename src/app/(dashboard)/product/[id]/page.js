import { getProductById } from '@/utils';
import { DetailProduct } from '@/components/clients/product';

const ProductDetail = async ({ params:{ id } }) => {
    const product = await getProductById({ id })
    return (
        <div className="max-w-[1440px] mx-auto h-screen flex flex-col">
            <DetailProduct data={product?.data}/>
        </div>
    )
}

export default ProductDetail