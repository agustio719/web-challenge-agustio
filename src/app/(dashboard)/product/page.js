import { Product } from '@/components/clients/product'
import { getProducts, getCategory, getProductsByCategory, getProductBySearch } from '@/utils';


const ProductPage = async ({ searchParams }) => {
    let limit = searchParams?.limit || 0
    let q = searchParams?.search 
    let category = searchParams?.category;
    
    const dataProductAll = await getProducts({ params: { limit } });
    const dataProductByCategory = await getProductsByCategory({ category });
    const dataProductBySearch = await getProductBySearch({ params: { q } })
    const dataCategory = await getCategory();

    let dataProduct = []
        if(q)  dataProduct = dataProductBySearch
        if(category)  dataProduct = dataProductByCategory
        else {
            dataProduct = dataProductAll
        }

    const [products, categories] = await Promise.all([dataProduct, dataCategory]);
    return (
        <div className="max-w-[1440px] mx-auto h-screen flex flex-col">
            <Product products={products?.data} categories={categories?.data} />
        </div>
    );
}

export default ProductPage;