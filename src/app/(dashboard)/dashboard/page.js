import { Dashboard } from '@/components/clients/dashboard';
import { getProducts, getCategory } from '@/utils';

const DashboardPage = async ({  }) => {

    let params = {
        limit: 0
    }
    const dataProduct = await getProducts({ params });
    const dataCategory = await getCategory();
    const [products, categories] = await Promise.all([dataProduct, dataCategory]);

    return (
        <div className="max-w-[1440px] mx-auto h-screen flex flex-col">
            <Dashboard 
                products={products?.data} 
                categories={categories?.data}
                
                />
        </div>
    );
    }

export default DashboardPage;