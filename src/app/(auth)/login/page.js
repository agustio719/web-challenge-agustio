import { Login } from '@/components/clients/auth';

const LoginPage = () => {
    return (
        <div className="max-w-[1440px] mx-auto h-screen flex flex-col justify-center items-center">
            <Login />
        </div>
    );
    }

export default LoginPage;
