'use server'
import { API, INSTANCE } from '@/configs/api';

export const auth = async (data) => {
    try{
        const res = await INSTANCE.post(API.login, data)
        return { data: res.data , status: res.status }
    }
    catch(error){
        return { error: error.response.data.message }
    }
       
}

export const editProductById = async ({ data, id }) => {
    try{
        const res = await INSTANCE.put(API.product + `/${id}`, data)
        return { data: res.data , status: res.status }
    }
    catch(error){
        return { error: error.response.data.message }
    }
       
}


