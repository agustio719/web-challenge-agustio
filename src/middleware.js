import { NextResponse } from 'next/server';

export default async function middleware(request) {
  let user = request.cookies.get('user')?.value
  const protectedPaths = ['/product','/dashboard'];
  if ((!user) && protectedPaths.some(path => request.nextUrl.pathname.includes(path))) {
    return NextResponse.redirect(new URL('/', request.nextUrl));
  }
    
  
}
 
export const config = {
  matcher: ['/((?!api|_next|_vercel|.*\\..*).*)', '/:locale/((?!api|_next|_vercel|.*\\..*).*)']

}

