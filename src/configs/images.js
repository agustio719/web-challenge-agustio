import Logo from '#/assets/images/logo.png';
import Illustration from '#/assets/images/illustration.png';

export const IMAGES = {
    LOGO : Logo,
    ILLUSTRATION : Illustration
}

export const ImageLoader = ({ src, width, quality }) => {
    return `${src}?w=${width}&q=${quality || 75}`
}