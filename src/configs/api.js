import axios from 'axios'

const baseUrl = process.env.NEXT_PUBLIC_API_URL 
export const INSTANCE = axios.create({
    baseURL: baseUrl,
    headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/json',
    }
})

export const API = {
    login: '/auth/login',
    product: '/products',
}