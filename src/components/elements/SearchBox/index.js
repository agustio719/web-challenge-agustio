'use client';
import styles from './styles.module.scss';
import { useState } from 'react';

const SearchBox = ({
    onSubmit = () => {},
    onKeyPress = () => {},
    ...props
    }) => {
    const [state, setState] = useState({
        value: '',
    });
    const { value } = state;
    const _handleChange = (e) => {
        setState({ ...state, value: e.target.value });
    }
    const _handleSubmit = (e) => {
        e.preventDefault();
        onSubmit(value);
    }
    const _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            onKeyPress(value);
        }
    }
    return (
        <div className={styles.searchBoxContainer}>
            <input 
                placeholder="Search"
                name='search'
                onChange={_handleChange}
                value={value}
                {...props}
                onKeyPress={_handleKeyPress}
                
            />
            <button className={styles.btnSearch} onClick={_handleSubmit}>
                Search
            </button>
        </div>
    );
    }

export default SearchBox;
