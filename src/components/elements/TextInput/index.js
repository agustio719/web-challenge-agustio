'use client';
import styles from './styles.module.scss';
import clsx from 'clsx';

const TextInput = ({
    label = 'label',
    placeholder = '',
    value = '',
    onChange = () => {},
    type = 'text',
    error = '',
    ...props
    }) => {
    return (
        <div className={styles.textInputContainer}>
            <label className={styles.label}>{label}</label>
            <div className={clsx(styles.wrapperInput, { [styles.error]:error })}>
                <input
                    placeholder={placeholder}
                    value={value}
                    onChange={onChange}
                    type={type}
                    {...props}
                />
            </div>
            {error && <p className={styles.error}>{error}</p>}
        </div>
    );
    }

export default TextInput;

