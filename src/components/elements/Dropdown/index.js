'use client';
import styles from './styles.module.scss';
import { useState, useEffect, useRef } from 'react';
import ArrowDown from '#/assets/icons/arrow-down';
import clsx from 'clsx';

const DropdownComponent = (props) => {
  const { label = '', placeholder = '', options = [], onChange = () => {}, value = '', className = '', variant } = props;
  const [state, setState] = useState({
    open: false,
  });
  const dropdownRef = useRef(null);
  const { open } = state;

  useEffect(() => {
    const handleClickOutside = () => {
      if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
        setState({ ...state, open: false });
      }
    };
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    }
  }, [state]);


  const toggleDropdown = () => setState({ ...state, open: !state.open });
  const handleSelected = (item) => {
    setState({ ...state, open: false });
    onChange(item);
  };
  
  const filterOptions = ()=> {
    let option = options?.filter((item) => item.value === value);
    return option[0]?.label;
  }
  return (
    <div className={clsx(styles.dropdownContainer, className)} ref={dropdownRef}>
      {label && <span className={styles.label}>{label}</span>}
      <div className={clsx(styles.wrapper, { 
        [styles.open]: open,
        [styles.variant2]: variant === 'variant2'
        })} onClick={toggleDropdown}>
      {filterOptions() || placeholder}
        <ArrowDown />
      {open && (
        <div className={styles.dropdown}>
          {options?.map((_, index) => (
            <div key={index} className={styles.item} onClick={() => handleSelected(_.value)}>
              <span>{_.label}</span>
            </div>
          ))}
          {!options && (
            <div className={styles.notFound}>
              <span>Data tidak ditemukan</span>
            </div>
          )}
        </div>
      )}
      </div>
    </div>
  );
};

export default DropdownComponent;