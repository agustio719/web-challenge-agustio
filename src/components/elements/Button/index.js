import styles from './styles.module.scss';
import clsx from 'clsx';
import { Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';

const ButtonComponent = ({
  variant = 'primary',
  children,
  disabled = false,
  className = '',
  loading = false,
    ...props
}) => {
  const buttonClasses = clsx(
    styles.buttonContainer,
    className,
    {
      [styles.primary]: variant === 'primary',
      [styles.outlined]: variant === 'outlined',
      [styles.primaryMaxWidth]: variant === 'primaryMaxWidth',
      [styles.outlinedMaxWidth]: variant === 'outlinedMaxWidth',
      [styles.outlinedBlack]: variant === 'outlinedBlack',
    }
  );

  return (
    <button 
      className={buttonClasses} 
      disabled={disabled}
      {...props}>
      {loading ? 
        <Spin 
          indicator={<LoadingOutlined />}
          size="small" /> : 
          children}
    </button>
  );
};

export default ButtonComponent;
