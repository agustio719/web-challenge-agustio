import styles from './styles.module.scss';
import Image from 'next/image';
import { ImageLoader } from '@/configs/images';
import { nominalRule } from '@/components/clients/product/detail-product';

const CardProduct = ({
    product,
    onClick = () => {},
    ...props
    }) => {
    return (
        <div className={styles.cardProductContainer} onClick={onClick} {...props}>
            <div className={styles.cardProductWrapper}>
                <div className={styles.cardProductImage}>
                    <Image 
                        loader={ImageLoader}
                        src={product?.thumbnail}
                        height={200}
                        width={200}
                        alt="product" />
                </div>
                <div className={styles.cardProductInfo}>
                    <h3 className={styles.title}>{product?.title}</h3>
                    <h2 className={styles.price}>Rp {nominalRule(product?.price)}</h2>
                    <div className={styles.discount}>
                        <h3 className={styles.normalPrice}>Rp {nominalRule(product?.normalPrice)}</h3>
                        <span>{product?.discountPercentage}%</span>
                    </div>
                    <span>{product?.rating} ⭐</span>
                </div>
            </div>
        </div>
    );
    }

export default CardProduct;