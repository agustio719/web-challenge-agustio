'use client'
import styles from './styles.module.scss';
import { useContext } from 'react';
import { ThemeContext } from '@/app/theme-provider';
import Image from 'next/image';
import { ImageLoader, IMAGES } from '@/configs/images';
import Link from 'next/link'
import { usePathname } from 'next/navigation';
import clsx from 'clsx';
import { useRouter } from 'next/navigation';
import Cookies from 'js-cookie';

const Navbar = () => {
    const { user } = useContext(ThemeContext);
    const pathname = usePathname()
    const router = useRouter()
    const path = [
        { link:'/dashboard', name:'Dashboard' },
        { link:'/product', name:'Product' }
    ]
    const _handelToggleLogout = () => {
        Cookies.remove('user')
        router.push('/')
    }
 
    return (
        <div className={styles.navbarContainer}>
            <div className={styles.navbarWrapper}>
                <div className={styles.navbarLeft}>
                    <Image 
                        src={IMAGES.LOGO}
                        loader={ImageLoader}
                        priority
                        height="auto"
                        width="auto"
                        alt="logo" />
                </div>
                <div className={styles.navbarContent}>
                    {path.map((item,index)=>(
                        <Link 
                            key={index} 
                            className={clsx(styles.link,{ [styles.active] : pathname.includes(item.link) })}
                            href={item.link}>{item.name}</Link>
                    ))}
                </div>
                <div className={styles.navbarRight}>
                    <span className={styles.navbarUsername}>{user?.username}</span>
                    <div className={styles.avatar}>
                        {user?.image && <Image 
                            src={user?.image}
                            loader={ImageLoader}
                            priority
                            height={40}
                            width={40}
                            alt="avatar" />
                        }
                    </div>
                    <button className={styles.logout} onClick={_handelToggleLogout}>Logout</button>
                </div>
            </div>
        </div>
    )
}

export default Navbar;