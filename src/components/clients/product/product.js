'use client';
import styles from './styles.module.scss';
import { CardProduct, SearchBox } from '@/components/elements';
import { Dropdown } from '@/components/elements';
import { useState, useEffect } from 'react';
import { useRouter, usePathname } from 'next/navigation';
import { Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';


const Product = (props) => {
    const { products, categories } = props;
    const [state, setState] = useState({
        category: '',
        limit: 'all',
        loading: false,
    });
    const { category, limit, loading } = state;
    const router = useRouter();
    const pathname = usePathname();
    const _handleChangeCategory = (value) => {
        setState({ ...state, category: value , limit: 'all', loading: true });
        router.push(`${pathname}?category=${value}`);
    }
    const _handleChangeLimit = (value) => {
        setState({ ...state, limit: value , category: '', loading: true });
        router.push(`${pathname}?limit=${value}`);
    }
    const _handleSearch = (value) => {
        setState({ ...state, loading: true });
        router.push(`${pathname}?search=${value}`);
    }
    const _handleToDetailProduct = (id) => {
        router.push(`/product/${id}`)
    }

    useEffect(() => {
        setState({ ...state, loading: false });
    }
    , [products]);

    const options = categories?.map((item) => {
        return {
            label: item,
            value: item,
        }
    }
    );
    const optionsLimit = [
        { label: 'All', value: '0' },
        { label: '10', value: '10' },
        { label: '20', value: '20' },
        { label: '30', value: '30' },
        { label: '40', value: '40' },
        { label: '50', value: '50' },
    ]
    let dataProduct = products?.products?.slice(0, 10).map((item) => {
        return {
            ...item,
            normalPrice: (item.price + (item.price * (item.discountPercentage / 100))).toFixed(0)
        }
    }
    );
    return (
        <div className={styles.productContainer}>
            <h1>Product Release</h1>
            <SearchBox  onSubmit={_handleSearch}  onKeyPress={_handleSearch} />
            <div className={styles.productFilter}>
                <Dropdown 
                    options={options} 
                    variant="variant2" 
                    label="Category" 
                    value={category} 
                    onChange={_handleChangeCategory} 
                />
                <Dropdown 
                    options={optionsLimit} 
                    variant="variant2" 
                    label="Limit" 
                    value={limit}
                    onChange={_handleChangeLimit}
                />
            </div>
            <div className={styles.productWrapper}>
                {!loading && dataProduct?.map((item) => {
                    return (
                        <CardProduct 
                            key={item.id}
                            onClick={()=>_handleToDetailProduct(item.id)}
                            product={item} />
                    )
                })}
            </div>
            {loading && (
                <div className={styles.loading}>
                    <Spin indicator={<LoadingOutlined style={{ fontSize: 24 }} spin />} />
                </div>
            )}
        </div>
    );
}

export default Product;
