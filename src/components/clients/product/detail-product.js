'use client'
import styles from './styles.module.scss'
import { useState, useEffect } from 'react';
import Image from 'next/image';
import { ImageLoader } from '@/configs/images';
import { Modal } from 'antd';
import { TextInput, Button } from '@/components/elements';
import { editProductById } from '@/app/actions'

export const nominalRule = (nominal) => {
    let reverse = nominal?.toString().split('').reverse().join('')
    let ribuan = reverse?.match(/\d{1,3}/g)
    ribuan = ribuan?.join('.').split('').reverse().join('')
    return ribuan
  }
const DetailProduct = (props) => {
    const { data } = props;
    const [state, setState] = useState({
        indexImage:0,
        showEditModal:false,
        showModalDelete:false,
        title:'',
        product:null,
        loading:false
    })
    const { indexImage, showEditModal, title, product , loading } = state; 
    useEffect(()=> {
        if(data) setState({ ...state, product:data })
    },[])
    
    const _handleChangeImage = (indexImage) => {
        setState({ ...state, indexImage })
    }
    const _handleToggleModal = () => setState({ ...state, showEditModal:!showEditModal, title:product?.title })
    const _handleChangeTitle = (e) => setState({ ...state, title: e.target.value })
    const _handelSubmitEdit = async (e) => {
        e.preventDefault()
        setState({ ...state, loading:true })
        let data = {
            title
        }
        const res  = await editProductById({ data, id:product.id })
        if(res.status === 200 ){
            setState({ ...state, product:res?.data , showEditModal:false })
        }
    }
    const renderModal = () => {
        return(
            <Modal
                open={showEditModal}
                onCancel={_handleToggleModal}
                footer={null}
                >   
                    <form onSubmit={_handelSubmitEdit}>
                        <TextInput 
                            label="Title"
                            value={title} 
                            onChange={_handleChangeTitle} 
                            name="title" 
                            />
                        <Button type="submit" loading={loading} disabled={loading}>Submit</Button>
                    </form>
                </Modal>
        )
    }
    let normalPrice = (data.price + (data.price * (data.discountPercentage / 100))).toFixed(0)

    return(
        <div className={styles.detailProductContainer}>
            {renderModal()}
            <div className={styles.imagesWrapper}>
                <div className={styles.imageActive}>
                    <Image 
                        loader={ImageLoader}
                        src={product?.images[indexImage]}
                        alt="image"
                        height={100}
                        width={100}
                        />
                </div>
                <div className={styles.imageDotsWrapper}>
                    {product?.images?.map((item,index) =>  (
                        <div className={styles.image} key={index} onClick={()=>_handleChangeImage(index)}>
                            <Image 
                                loader={ImageLoader}
                                src={item}
                                alt="image"
                                height={50}
                                width={50}
                                />
                            </div>
                    ))}
                </div>
            </div>
            <div className={styles.productWrapper}>
                <h1>{product?.title}</h1>
                <div >⭐ {product?.rating} • {product?.stock} (PCS)</div>
                <h2>Rp. {nominalRule(product?.price)}</h2>
                <h2 className={styles.normalPrice}>Rp. {nominalRule(normalPrice)}</h2>
                <h3>Descriptions:</h3>
                <p>{product?.description}</p>
                <p>Brand: {product?.brand}</p>
                <div className={styles.btnWrapper}>
                    <button className={styles.edit} onClick={_handleToggleModal}>Edit</button>
                </div>

            </div>
            
            
        </div>
    )
}

export default DetailProduct