import Product from './product';
import DetailProduct from './detail-product';

export {
    Product,
    DetailProduct
}