'use client'
import styles from './styles.module.scss';
import { CardProduct } from '@/components/elements';
import { useRouter } from 'next/navigation';

const Dashboard = (props) => {
    const { products, categories } = props;
    const router = useRouter();
    let ratingAvg = 0;
    let ratingTotal = products?.products?.reduce((acc, cur) => {
        return acc + cur.rating;
    }
    , 0);
    ratingAvg = (ratingTotal / products?.products?.length).toFixed(1);
    let totalStock = products?.products?.reduce((acc, cur) => {
        return acc + cur.stock;
    }
    , 0);
    

    let dataRow1 = [
        {
            title: 'Total Products',
            value: products?.total
        },
        {
            title: 'Total Categories',
            value: categories?.length

        },
        {
            title: 'Rating Avg',
            value: ratingAvg

        },
        {
            title: 'Total Stock',
            value: totalStock

        }
    
    ]
  
    let dataProduct = products?.products?.slice(0, 10).map((item) => {
        return {
            ...item,
            normalPrice: (item.price + (item.price * (item.discountPercentage / 100))).toFixed(0)
        }
    }
    );
    const handleSeeAll = () => {
        router.push('/products')
    }
    const _handleToDetailProduct = (id) => {
        router.push(`/product/${id}`)
    }
    return (
        <div className={styles.containerDashboard}>
            <h3>Overview</h3>
            <div className={styles.wrapperDashboard}>
                {dataRow1?.map((item, index) => (
                    <div className={styles.wrapper} key={index}>
                        <h3>{item.title}</h3>
                        <h1>{item.value}</h1>
                    </div>
                ))}
            </div>
            <div className={styles.wrapperProduct}>
                <div className={styles.wrapperTitle}>
                    <h3>Products</h3>
                    <button onClick={handleSeeAll}>See All</button>
                </div>
                <div className={styles.wrapperCardProduct}>
                    {dataProduct?.map((item, index) => (
                        <CardProduct 
                            key={index}
                            onClick={()=>_handleToDetailProduct(item.id)}

                            product={item} />
                    ))}
                </div>
            </div>
        </div>
    );
    }

export default Dashboard;