'use client'
import styles from './styles.module.scss';
import Image from 'next/image';
import { IMAGES, ImageLoader } from '@/configs/images';
import { TextInput, Button } from '@/components/elements';
import Link from 'next/link';
import { Formik } from 'formik';
import { message } from 'antd';
import { auth } from '@/app/actions';
import { useContext } from 'react';
import { ThemeContext } from '@/app/theme-provider';
import { useRouter } from 'next/navigation';

const LoginPage = () => {
    const { setUser } = useContext(ThemeContext);
    const router = useRouter();
    const _handleLogin = async (values, { setSubmitting, setFieldError }) => {
        const { username, password } = values;
        if (!username) {
            const errorMessage = 'User ID is required';
            setFieldError('username', errorMessage);
            setSubmitting(false);
            message.error(errorMessage);
            return;
        }
        if (!password) {
            const errorMessage = 'Password is required';
            setFieldError('password', errorMessage);
            setSubmitting(false);
            message.error(errorMessage);
            return;
        }
        const body = JSON.stringify({
            username,
            password
        });
       const res = await auth(body)
        if(res.status === 200){
            setUser(res.data)
            return router.push('/dashboard')
        }else {
            message.error(res.error)
            setSubmitting(false);
        }
    }

    const renderForm = (props) => {
        const { values, handleChange, handleSubmit:_handleLogin, isSubmitting, errors } = props;
        const { username, password } = values;
        return (
            <form onSubmit={_handleLogin}>
                <h4>Login</h4>
                <p className={styles.label}>Please sign in to continue.</p>
                <TextInput 
                    label="User ID"
                    placeholder="User ID"
                    value={username}
                    onChange={handleChange} 
                    error={errors.username}
                    name="username" />
                <TextInput 
                    label="Password"
                    placeholder="Password"
                    value={password} 
                    onChange={handleChange} 
                    error={errors.password}
                    type="password"
                    name="password" />
                <div className={styles.btnWrapper}>
                    <Button 
                        variant="primaryMaxWidth"
                        type="submit"
                        loading={isSubmitting}
                        disabled={isSubmitting}>
                        Login
                    </Button>
                </div>
            </form>
        )
    }
    return (
        <div className={styles.containerLogin}>
            <Image
                loader={ImageLoader}
                className={styles.illustrasion}
                src={IMAGES.ILLUSTRATION}
                alt="background"
                priority
            />
            <div className={styles.loginWrapper}>
                <div className={styles.background}/>
                <div className={styles.formLogin}>
                    <Image
                        loader={ImageLoader}
                        src={IMAGES.LOGO}
                        alt="logo"
                        height="auto"
                        width="auto"
                        priority
                    />
                    <Formik
                        component={renderForm}
                        initialValues={{
                            username: '',
                            password: ''
                        }}
                        onSubmit={_handleLogin}
                     />
                    <div className={styles.footer}>
                        <p>Dont have an account? <Link href="#">Sign Up</Link></p>
                    </div>

                </div>
            </div>
        </div>
    );
    }

export default LoginPage;