/** @type {import('next').NextConfig} */
const path = require('path')

const nextConfig = {
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
  eslint: {
    ignoreDuringBuilds: true,
  },
 
  async rewrites() {
    return [
      {
        source: '/',
        destination: '/login',
      }
    ]
  },
    swcMinify: false ,
    experimental: {
      serverActions: true,
    },
}




module.exports = nextConfig