const plugin = require('tailwindcss/plugin')
const fonts = require('./public/assets/styles/font')

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      colors: {
        primary: '#7E00B3',
        secondary: '#FD5914',
        grey: '#E5E5E5',
      }
    },
    screens: {
      'md': { 'max': '1439px' },
      'xl': { 'max': '1279px', 'min': '1021px' },
      'sm': { 'max': '1020px' },
      'xs': { 'max': '767px' }
    },
  },
  plugins: [
    require('tailwindcss-animate'),
    plugin(function ({ addComponents }) {
     
      addComponents(fonts)

    }),
  ],
}
